import React, {useEffect, useState, Component} from 'react';
import styled from 'styled-components';
import axios from 'axios';
import Home from "./Home";
const Title = styled.p`
    font-size:30px;
    font-weight:600;
    font-family:Arial Rounded MT ;
    //background:#f1f1f1;
    
`;
const Bodiy=styled.div`
   display:flex;
   width: 300px;
   position:relative;
   bottom:240px;
   margin:0 auto;
   z-index:10;
   flex-direction:column;
`
const Main=styled.div`
    width:600px;
    height:700px;
    margin: 0 auto;
    margin-top:20px;
    background:#f1f1f1;
    border:1px solid white;
    border-radius: 0px 50px 0px 50px;
    
`
const Label=styled.label`
    font-family:Arial Rounded MT;
    font-size:14px;
    font-weight:10;
    color:#4c4b4b;
    margin:0;
    margin-bottom:5px;
`
const Input=styled.input`
    background:#e3e6ec;
    height: 50px;
    color:#000;
    
    border-radius:7px;
    border:0px;
    box-shadow: -5px -4px 10px 0px #fff, 5px 4px 10px 0px rgba(88,88,88,0.435);
    padding-left:15px;
    margin-bottom:30px;
`
const ViewButton=styled.span`
    position: relative;
    width: 43px;
    height: 18px;
    bottom:63px;
    left:250px;
    
    font-family: Arial Rounded MT;
    font-style: normal;
    font-weight: 500;
    font-size: 12px;
    line-height: 17px;
    color: #4785D6;
`
const ForgotTitle=styled.p`
    font-family: Arial Rounded MT;
    font-size: 14px;
    line-height: 17px;
    color: #4785D6;
    display:flex;
    justify-content:flex-end;
`
const LoginButton=styled.button`
    width:100%;
    baclground:#4785D6;
    font-family: Arial Rounded MT;
`
const Sircle1=styled.div`
    width:100px;
    height:100px;
    border-radius:50%;
    box-shadow: -5px -4px 10px 0px #fff, 5px 4px 10px 0px rgba(88,88,88,0.435);
    
    position:relative;
    top:30px;
    left:30px;
    
    
`
const Sircle2=styled.div`
    width:100px;
    height:100px;
    border-radius:50%;
    box-shadow: -5px -4px 10px 0px #fff, 5px 4px 10px 0px rgba(88,88,88,0.435);
    
    position:relative;
    left:400px;
  
    
    
`
const Sircle3=styled.div`
    width:80px;
    height:80px;
    border-radius:50%;
    box-shadow: -5px -4px 10px 0px #fff, 5px 4px 10px 0px rgba(88,88,88,0.435);
    
    position:relative;
    left:190px;
    bottom:60px;
  
    
    
`
const Square1=styled.div`
    width:200px;
    height:100px;
    border-radius:10px;
    box-shadow: -5px -4px 10px 0px #fff, 5px 4px 10px 0px rgba(88,88,88,0.435);
    
    position:relative;
    left:350px;
    bottom:350px;
`
const Square2=styled.div`
    width:120px;
    height:100px;
    border-radius:10px;
    box-shadow: -5px -4px 10px 0px #fff, 5px 4px 10px 0px rgba(88,88,88,0.435);
    
    position:relative;
    left:80px;
    bottom:510px;

`

const Points1=styled.p`
    font-size:30px;
    width:66px;
    height:100px;
    line-height:0.4;
    letter-spacing:3px;
    color:#eae2e2;
    z-index:12;
    position:relative;
    bottom:90px;
    left:370px;
`
const Points2=styled.p`
    font-size:30px;
    width:66px;
    height:113px;
    line-height:0.4;
    letter-spacing:3px;
    color:#eae2e2;
       
    position:relative;
    bottom:0px;
    left:80px;
`
export default  function LoginPage (){

    const  [loggedIn,setLoggedIn]=useState();
    const onSubmit = async  e =>{
        e.preventDefault();
        let request={
            login: document.getElementById("exampleInputName").value,
            pass: document.getElementById("exampleInputPassword").value
        }
        axios.post("http://134.209.202.19/api/login",request)
            .then(resp =>{
                {
                    console.log(resp);
                    setLoggedIn(true);
                }
            })
            .catch(error=>{
                alert(error);
            })
    }
    return(
        <>
            {loggedIn?<Home/>: <Main>
                <Sircle1></Sircle1>
                <Sircle2></Sircle2>
                <Sircle3></Sircle3>
                <Points1>
                    ...... ...... ...... ...... ...... ...... ......
                </Points1>
                <Bodiy>
                    <Title>Hello. <br/> Welcome Back</Title>
                    <div>
                        <form onSubmit={onSubmit}>
                            <div style={{display:'flex', flexDirection:'column'}}>
                                <Label >Name</Label>
                                <Input
                                    type="text"
                                    plaseholder="Name"
                                    name="login"
                                    // value={this.state.login}
                                    // onchange={this.handleChange}
                                    id="exampleInputName"/>
                            </div>
                            <div style={{display:'flex', flexDirection:'column'}}>
                                <Label >Password</Label>
                                <Input
                                    plaseholder="Passwod"
                                    name="pass"
                                    // value={this.state.pass}
                                    type="password"
                                    // onchange={this.handleChange}
                                    id="exampleInputPassword"
                                    //type={view ? 'password' : 'text'}
                                />
                                <ViewButton >VIEW</ViewButton>
                            </div>
                            <ForgotTitle>Forgot Password?</ForgotTitle>
                            <LoginButton
                                type="submit"
                                className="btn btn-primary"

                            >
                                LOGIN</LoginButton>
                        </form>
                    </div>
                    <Points2>
                        ...... ...... ...... ...... ...... ...... ...... ...... ...... ......
                    </Points2>
                </Bodiy>

                <Square1></Square1>
                <Square2></Square2>
            </Main>
            }

            </>
    );
}
